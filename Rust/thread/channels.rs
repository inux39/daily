use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;

static NTHREADS: i32 = 4;

fn main() {
	// チャネルにはSender<T>, Receiver<T>の2つのエンドポイントがある
	let (tx, rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();

	for id in 0..NTHREADS {
		// 送信者エンドポイントをコピー
		let thread_tx = tx.clone();
		// IDを送信
		thread::spawn(move || {
			thread_tx.send(id).unwrap();
			// メッセージを送信した後もすぐに実行継続
			println!("thread {} finished", id);
		});
	}
	// 全てのメッセージを収集
	let mut ids = Vec::with_capacity(NTHREADS as usize);
	for _ in 0..NTHREADS {
		ids.push(rx.recv());
	}
	// メッセージが送信された順番
	println!("{:?}", ids);
}

