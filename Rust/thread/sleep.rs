
use std::time::{Duration, SystemTime};
use std::thread::sleep;

fn main() {
	let wait_sec: u64 = 1;
//	let wait_nano: u32 = 500000;
	let wait_nano: u32 = 0;
	let nano: Vec<u32> = Vec::new();
	for i in 0..10 {
		let now = SystemTime::now();
		println!("{:?}", now);
//		nano.push(now.tv_nsec);
		println!("{}: wait {}.{}sec", i, wait_sec, wait_nano);
		sleep(Duration::new(wait_sec, wait_nano));
	}
	/*
	for i in 0..10 {
		if i < 9 {
			println!("{}", nano[i + 1] - nano[i]);
		}
	}
	*/
}

