
use std::thread;
// threadの数
static NTHREADS: i32 = 10;
fn main() {
	// spawnされるクロージャを保持するベクタ
	let mut children = vec![];

	for i in 0..NTHREADS {
		// あたらしいスレッドを起動
		children.push(thread::spawn(move || {
			println!("this is thread number {}", i)
		}));
	}

	for child in children {
		// 子スレッドが終了するのを待って、結果を返す
		let _ = child.join();
	}
}

