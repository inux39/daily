use std::fmt::Debug;
use std::ops::Add;
struct Point<T> {
	x: T,
	y: T,
}

// メソッドの定義
trait Items<T> {
	fn total(self) -> T;
}

// トレイトの実装
// 'T'はstd::ops::Addを実装している必要があることを規定している
	// <- ジェネリック境界
impl<T: Add<Output=T>> Items<T> for Point<T> {
	fn total(self) -> T {
		self.x + self.y
	}
}

fn main() {
	some(1);
	same(10);
	let p = Point {
		x: 10,
		y: 30,
	};
	println!("{}", p.total());
}

// Debugが実装されているか
fn some<T: Debug>(x: T) {
	println!("{:?}", x);
}

// ↑ と同じものを where で見やすくできる
fn same<T>(x: T) where T: std::fmt::Display {
	println!("{}", x);
}

