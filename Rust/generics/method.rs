
struct Point<T> {
	x: T,
	y: T,
	z: T,
}

// <T>: ジェネリックパラメータ
impl<T> Point<T> {
	pub fn new(x: T, y: T, z: T) -> Point<T> {
		Point {
			x: x,
			y: y,
			z: z,
		}
	}
}

// 型を指定してメソッドを実装
impl Point<f64> {
	pub fn ground(&self) -> bool {
		if self.z > 0.0 {
			true
		} else {
			false
		}
	}
	pub fn show(&self) {
		println!("x: {}, y: {}, z: {}", self.x, self.y, self.z);
	}
}

impl Point<i64> {
	pub fn ground(&self) -> bool {
		if self.z > 0 {
			true
		} else {
			false
		}
	}
	pub fn show(&self) {
		println!("x: {}, y: {}, z: {}", self.x, self.y, self.z);
	}
}

fn main() {
	let p = Point::new(10, 10, 10);
	p.show();
	println!("ground: {}", p.ground());
	let f = Point::new(0.0, 0.0, 5.0);
	f.show();
	println!("ground: {}", f.ground());
}

