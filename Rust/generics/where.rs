//impl <A: TraitB + TraitC, D: TraitE + TraitF> MyTrait<A,D> for YourType {}
//		おなじ
//impl <A, D> MyTrait<A, D> for YourType where
//	A: TraitB + TraitC,
//	D: TraitE + TraitF {}

// whereなしで表現できないやつ
use std::fmt::Debug;

trait PrintInOption {
	fn print_in_option(self);
}

// whereを使わない場合 <T: Debug>かそれ以外の直接的でない方法しかない
impl<T> PrintInOption for T where
	Option<T>: Debug {
	fn print_in_option(self) {
		println!("{:?}", Some(self));
	}
}

fn main() {
	let vec = vec![1, 2, 3];
	vec.print_in_option();
}

