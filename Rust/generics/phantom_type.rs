use std::marker::PhantomData;
// 幽霊型(Phantom Type)とは実行時には存在しないけれども、
// コンパイル時に静的に型チェックされるような型のことです。
#[derive(PartialEq, Debug)]
struct PhantomTuple<A, B>(A, PhantomData<B>);

#[derive(PartialEq, Debug)]
struct PhantomStruct<A, B> {
	first: A,
	phantom: PhantomData<B>,
}

fn main() {
	let _t1: PhantomTuple<char, f32> = PhantomTuple('Q', PhantomData);
	let _t2: PhantomTuple<char, f64> = PhantomTuple('Q', PhantomData);

	let _s1: PhantomStruct<char, f32> = PhantomStruct {
		first: 'Q',
		phantom: PhantomData,
	};
	let _s2: PhantomStruct<char, f64> = PhantomStruct {
		first: 'Q',
		phantom: PhantomData,
	};
	println!("{:?}", _t1);
	println!("{:?}", _t2);
	println!("{:?}", _s1);
	println!("{:?}", _s2);
	/*
	_t1 = <char, f32>
	_t2 = <char, f64>
	println!("t1 == t2: {}", _t1 == _t2);
                             ^ f32  ^f64
	型がちがうので、比較することができない
	*/
}

