struct A;			// 抽象型 `A`
struct S(A);		// 具象型 `S`
struct SG<T>(T);		// ジェネリクス型 `T`

fn reg(_s: S) {}
fn gen_t(_s: SG<A>) {}
fn gen_i32(_s: SG<i32>) {}
fn generics<T>(_s: T) {}	// ジェネリックな関数

fn main() {
	reg(S(A));
	gen_t(SG(A));
	gen_i32(SG(2));
	generics::<char>('i');
//	generics::<char>(SG('i'));
	generics(SG('i'));
}

