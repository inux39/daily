use std::fmt::{Debug, Display};

// DebugとDisplayトレイトを実装しているTを引数としてとる。
fn compare_prints<T: Debug + Display>(t: &T) {
	println!("Debug:   {:?}", t);
	println!("Display: {}", t);
}

// Debugトレイトを実装しているT,Uを引数としてとる。
fn compare_types<T: Debug, U: Debug>(t: &T, u: &U) {
	println!("t: {:?}", t);
	println!("U: {:?}", u);
}

fn main() {
	let string = "words";
	let array = [1, 2, 3];
	let vec = vec![1, 2, 3];

	compare_prints(&string);
	// arrayにはDisplayが実装されていないので、コンパイルエラー
	//compare_prints(&array);
	compare_types(&array, &vec);
}

