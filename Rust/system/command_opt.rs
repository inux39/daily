
fn main() {
	/// command line argument list
	let args: Vec<String> = std::env::args().collect();
	/// print all arguments
	for s in &args {
		println!("{}", s);
	}

	println!("");
	/// Different by OS environment
	for argument in std::env::args_os() {
		println!("{:?}", argument);
	}

	println!("");
	/// if args exists "-i"
	if args.len() > 1 && args[1].eq("-i") {
		println!("-i");
	}
}

