
fn main() {
	/// current_dir は端末なんかから起動したときの場所
	let current_dir = std::env::current_dir().unwrap();
	println!("{}", current_dir.display());

	/// current_exe は実行ファイルの場所
	let current_exe = std::env::current_exe().unwrap();
	println!("{}", current_exe.display());

	/// joinでOSに合うように繋げてくれる
	let current_exe_extra =
		std::env::current_exe().unwrap().join("test.txt");
	println!("{}", current_exe_extra.display());
}

