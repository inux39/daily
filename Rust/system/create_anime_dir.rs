
fn main() {
	let season = ["冬", "春", "夏", "秋", "年末"];
	let mut path = std::path::PathBuf::new();
	// parent directory
	println!("Parent directory: ");
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let dir = s.trim();
	path.push(&dir);

	// year directory
	println!("Year: ");
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let year = s.trim();
	path.push(&year);
	match std::fs::create_dir(&path) {
		Ok(_) => println!("created {:?}", path),
		Err(e) => println!("{:?}", e),
	}

	// season directory
	for i in 0..season.len() {
		let dir = format!("{}-{}-{}", year, i + 1, season[i]);
		let mut paths = path.clone();
		paths.push(&dir);
		match std::fs::create_dir(&paths) {
			Ok(_) => println!("created {:?}", paths),
			Err(e) => println!("{:?}", e),
		}
	}
}

