
#[cfg(target_os = "linux")]
fn main() {
	let cmd = std::process::Command::new("sh")
		.arg("-c")
		.arg("echo $EDITOR")
		.output()
		.expect("failed to execute process");
	println!("{}", String::from_utf8_lossy(&cmd.stdout));
	/*
	let stdout = String::from_utf8_lossy(&cmd.stdout);
	let stderr = String::from_utf8_lossy(&cmd.stderr);
	print!("stdout:\n{}", stdout);
	if stdout.is_empty() {
		println!("EMPTY");
	} else {
		println!("");
	}
	print!("stderr:\n{}", stderr);
	if stderr.is_empty() {
		println!("EMPTY");
	} else {
		println!("");
	}
	*/
}

