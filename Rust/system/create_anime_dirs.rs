use std::path::Path;
use std::fs;
fn main() {
	let current = std::env::current_dir().unwrap();
	let path = Path::new(&current);
	for i in fs::read_dir(path) {
		println!("{:?}", i.next());
	}
}

