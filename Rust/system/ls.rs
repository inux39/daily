use std::fs;

fn main() {
	let current = std::env::current_dir().unwrap();
	for entry in fs::read_dir(current).unwrap() {
		let dir = entry.unwrap();
		println!("{:?}", dir.file_name());
	}
}

