use std::io::Read;
fn main() {
	let path = std::path::Path::new(".");
	let new_path = path.join("test.txt");
	match new_path.to_str() {
		None => panic!("new_path is not a valid UTF8 sequence"),
		Some(s) => println!("new_path is {}", s),
	}

	let mut file = match std::fs::File::open(&new_path) {
		Err(why) => panic!("Could'nt open {}: {}",
				new_path.display(),
				std::error::Error::description(&why)),
		Ok(file) => file,
	};

	let mut file_s = String::new();
	match file.read_to_string(&mut file_s) {
		Err(why) => panic!("Couldn't read {}: {}",
				new_path.display(),
				std::error::Error::description(&why)),
		Ok(_) => {},
	}
	println!("{}", file_s);
}

