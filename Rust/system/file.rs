
use std::io::{Read, Write};
use std::error::Error;

fn main() {
	let filename = "";

	// read
	let mut buffer = std::io::BufReader::new(file);
	let mut s = String::new();
	buffer.read_to_string(&mut s).unwrap();
	println!("{}", s.trim());
}
/*
fn read(f: std::fs::File) {

}

fn write() {
	let file_name = "test.txt";
	let file = match std::fs::OpenOptions::new()
		.write(true).append(true).open(file_name) {
		Err(why) => panic!("{}", why.description()),
		Ok(o) => o,
	};
	let mut buffer = std::io::BufWriter::new(file);
	buffer.write(b"test\n").unwrap();
}
*/

