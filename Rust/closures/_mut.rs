
pub fn exec() {
	println!("_mut");
	let mut count = 0;
	let mut inc = || {
		count += 1;
		println!("`count` = {}", count);
	};
	inc();
	inc();
	inc();
	// moveされるのでエラーになる
//	let reborrow = &mut count;
	use std::mem;
	let movable = Box::new(10);
	let consume = || {
		println!("`movable = {}`", movable);
		mem::drop(movable);
	};
	consume();
	// movableはdropされたので無理
//	consume();

	println!("");
}

