// https://keens.github.io/blog/2016/10/10/rustnokuro_ja3tanewotsukutterikaisuru/
// https://doc.rust-jp.rs/rust-by-example-ja/fn/closures.html
mod simple;
mod plus_one;
mod color;
mod _mut;

fn main() {
	simple::exec();
	plus_one::exec();
	color::exec();
	_mut::exec();
}

