
fn main() {
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let n = s.trim().parse::<usize>().unwrap();
	for i in n..100 {
		println!("{}", fizzbuzz(i));
	}
}

fn fizzbuzz(n: usize) -> String {
	let mut s = String::new();
	if n % 3 == 0 {
		s += "Fizz";
	}
	if n % 5 == 0 {
		s += "Buzz";
	}
	if s.is_empty() {
		s = format!("{}", n);
	}
	s
}

#[test]
fn fb_test() {
	assert!(fizzbuzz(1) == "1");
	assert!(fizzbuzz(9) == "Fizz");
	assert!(fizzbuzz(15) == "FizzBuzz");
	assert!(fizzbuzz(55) == "Buzz");
}
