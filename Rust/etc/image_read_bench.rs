use std::io::{Read, BufReader, stdout, Write, BufWriter};
use std::fs::{self, File};

fn main() {
    let out = stdout();
    let mut out = BufWriter::new(out.lock());
    for e in fs::read_dir(&std::env::current_dir().unwrap()).unwrap() {
        let path = e.unwrap().path();
        if path.is_file() {
            let file = File::open(&path).unwrap();
            let mut f = BufReader::new(file);
            let mut data = Vec::new();
            f.read_to_end(&mut data).unwrap();
            write!(out, "{:?} : {} bytes\n", path.file_name().unwrap(), data.len()).unwrap();
        }
    }
}

