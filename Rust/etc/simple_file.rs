mod file_io;
use file_io::*;
use std::path::Path;

const FILE: &'static str = "file.log";

fn main() {
	let path = Path::new(FILE);
	let mut buf = String::new();
	std::io::stdin().read_line(&mut buf).expect("stdin Error");

	append(path, buf).unwrap();

	let file = read_to_string(path).unwrap();
	println!("{}", file);
}

