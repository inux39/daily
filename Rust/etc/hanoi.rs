fn main() {
	hanoi(3, "A", "B", "C");
}

fn hanoi(n: i32, from: &str, work: &str, dest: &str) {
	if n >= 1 {
		hanoi(n - 1, from, dest, work);
		println!("{}を{}から{}へ", n, from, dest);
		hanoi(n - 1, work, from, dest);
	}
}

