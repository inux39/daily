
use std::io::{Read, Write, BufWriter};
use std::fs::{File};
use std::path::Path;

fn main() {
	let out = std::io::stdout();
	let mut out = BufWriter::new(out.lock());

	let mut name = String::new();
	std::io::stdin().read_line(&mut name).expect("stdin Error");
	let path = Path::new(name.trim());
	// ファイルの内容を表示する
	let mut buf = String::new();
	let mut file = match File::open(&path) {
		Err(e) => panic!("{:?}", e.kind()),
		Ok(f) => f,
	};
	let mut buffer: Vec<u8> = Vec::new();
	file.read_to_end(&mut buffer)
		.map_err(|error| panic!("{:?}", error.kind()));
	for x in &buffer {
		write!(out, "0x{:02X}", x).unwrap();
	}
	writeln!(out, "").unwrap();
}

