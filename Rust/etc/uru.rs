
fn main() {
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let y: usize = s.trim().parse().unwrap();
	println!("{}年は{}", y, to_str(is_uru(y)));
}

fn is_uru(y: usize) -> bool {
	if y % 4 == 0 {
		if y % 100 == 0 && y % 400 != 0 {
			false
		} else {
			true
		}
	} else {
		false
	}
}

fn to_str(b: bool) -> &'static str {
	match b {
		true => "うるう年",
		false => "うるう年ではない",
	}
}

#[test]
fn test_is_uru() {
	assert!(is_uru(1600) == true);
	assert!(is_uru(1700) == false);
	assert!(is_uru(2000) == true);
	assert!(is_uru(2500) == false);
}

#[test]
fn test_to_str() {
	assert_eq!(to_str(true), "うるう年");
	assert_eq!(to_str(false), "うるう年ではない");
}

#[test]
fn test_combo() {
	assert_eq!(to_str(is_uru(2000)), "うるう年");
	assert_eq!(to_str(is_uru(2500)), "うるう年ではない");
}
/*
*/

