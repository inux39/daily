//! [エラーハンドリング](https://rust-lang-ja.github.io/the-rust-programming-language-ja/1.6/book/error-handling.html)

use std::io::{Write, BufWriter};

#[cfg(not(test))]
fn main() {
	let out = std::io::stdout();
	let mut out = BufWriter::new(out.lock());

	let mut buf = String::new();
	match std::io::stdin().read_line(&mut buf) {
		Ok(s) => writeln!(out, "{} bytes", s).expect("stdout error"),
		Err(e) => writeln!(out, "error: {}", e).expect("stdout error"),
	};
	let n: Option<i32> = match buf.trim().parse() {
		Ok(i) => Some(i),
		Err(_) => None,
	};
	match n {
		Some(i) => writeln!(out, "{}", i).expect("stdout error"),
		None => {},
	};
	n.map(|x| writeln!(out, "{}", x).expect("stdout error"));
	// Noneのときにおちるやつ。
	//println!("{}", n.unwrap());
	match test(n.unwrap()) {
		Ok(i) => writeln!(out, "{}", i).expect("stdout error"),
		Err(e) => writeln!(out, "{}", e).expect("stdout error"),
	};

}

fn test(i: i32) -> Result<i32, String> {
	if i < 30 {
		Err("under 30".to_string())
	} else {
		Ok(i * 10)
	}
}

#[test]
fn testing() {
	assert!(test(0) == Err("under 30".to_string()));
	assert!(test(100) == Ok(1000));
}

