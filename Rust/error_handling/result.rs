
use std::error::Error;
use std::num::ParseIntError;
fn main() {
	// OK!
	println!("{}兆円欲しい!!!", str_to_i32("5000"));
	// panic!
	// str_to_i32("abc");
	puts(better_i32("10"));
	puts(better_i32("ABC"));
}

fn str_to_i32(s: &str) -> i32 {
	s.parse::<i32>().unwrap()
}

fn better_i32(s: &str) -> Result<i32, ParseIntError> {
	// Resultにmapすることもできる
	s.parse::<i32>().map(|n| n)
	// おなじ
	/*
	match s.parse::<i32>() {
		Ok(n) => Ok(n),
		Err(e) => Err(e),
	}
	*/
}

fn puts(r: Result<i32, ParseIntError>) {
	match r {
		Ok(n) => println!("{}", n),
		Err(e) => println!("Error: {}", e.description()),
	}
}

