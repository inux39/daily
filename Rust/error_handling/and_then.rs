
fn main() {
	let x = Some(10);
	x.and_then(square).map(|x| {
		println!("{}", x);
	});
	let y = Some(10);
	y.and_then(none).map(|y| {
		println!("{}", y);
	});
}

fn none(_: i32) -> Option<i32> {
	None
}

fn square(x: i32) -> Option<i32> {
	Some(x * x)
}

