//! # Option Sample
//! MoneyForward
//! Optional(2018)年あけましておめでとうございます

fn main() {
	let y = Some(2018);
	moneyforward(&y);
	looks_good(&None::<i32>);
}

fn moneyforward(y: &Option<i32>) {
	println!("{:?}年あけましておめでとうございます。", y);
	println!("{}年を振り返って今年1年の家計の計画を立てませんか",
	y.unwrap() - 1);
}

fn looks_good(y: &Option<i32>) {
	match *y {
		Some(n) => {
			println!("{}年あけましておめでとうございます。", n);
			println!("{}年を振り返って今年1年の家計の計画を立てませんか", n-1);
		},
		None => {
			println!("あけましておめでとうございます。");
			println!("今年1年の家計の計画を立てませんか");
		},
	}
}

