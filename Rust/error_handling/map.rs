
fn main() {
	let x = Some("str");
	match x {
		Some(t) => println!("match {}", t),
		None => println!("match None!"),
	};
	// ↑ と(ほぼ)おなじことをする
	x.map(|x| println!("{}", x));
	let y = None::<&str>;
	// map_or(DEFAULT_VAL, |v| fn)
	println!("{}", y.map_or("None!", |y| y));
}

