
fn main() {
	let mut v: Vec<i64> = Vec::new();

	for i in 0..10 {
		v.push(i);
	}
	println!("Normal");
	for i in &v {
		println!("{}", i);
	}
	println!("Reverse");
	for i in (0..v.len()).rev() {
		println!("{}", v[i]);
	}
}

