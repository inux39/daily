
struct Present {
	num: i32,
	unit: &'static str,
}

impl Drop for Present {
	fn drop(&mut self) {
		println!("Drop: {}{}", self.num, self.unit);
		// ここでdropされる
	}
}

fn main() {
	let present = Present {num: 5000, unit: "兆円"};
	println!("貰えないバージョン");
	println!("{}{}欲しい!!!", present.num, present.unit);
	drop(present);
	// dropされたのでエラーとなる
//	println!("{}{}貰えなかった", present.num, present.unit);

	let present2 = Present {num: 5000, unit: "兆円"};
	println!("\n貰えるバージョン");
	println!("{}{}欲しい!!!", present2.num, present2.unit);
	drop(&present2);
	println!("{}{}貰える", present2.num, present2.unit);
	// ここでpresent2はdropされる
}

