
fn main() {
	println!("200~1000以下の奇数を2乗した値の合計を求める");
	let max = 1000;
	// http://rust-lang-ja.org/rust-by-example/fn/hof.html
	// アプローチその1
	let mut acc = 0;
	// 無限
	for n in 0.. {
		let n_squared = n * n;
		if n_squared >= max {
			break;
		} else if is_odd(n_squared) {
			// 奇数のとき
			acc += n_squared;
		}
	}
	println!("{}", acc);

	// アプローチその2
	let sum_of_squared_odd_numbers: u32 =
		(0..).map(|n| n * n)
		.take_while(|&n| n < max)	// 2乗した値がmaxよりも小さいもので
		.filter(|n| is_odd(*n))		// 奇数のものを
		// fold(初期値, |累積, 要素|, 式)
		// この場合、0で初期化し累積+奇数をする。
		.fold(0, |sum, i| sum + i);	// たす
	println!("{}", sum_of_squared_odd_numbers);
}

fn is_odd(n: u32) -> bool {
	n % 2 == 1
}

