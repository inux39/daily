//! # [trait](https://rust-lang-ja.github.io/the-rust-programming-language-ja/1.6/book/traits.html)
//! トレイトは型が提供しなければならない機能をコンパイラに教える機能
//!
//! `impl`に似ているが、関数本体を定義するのではなく、型を定義する。
// TODO: 継承, trait object

#[allow(non_camel_case_types)]
struct classic_Phone {
	number: u32,
	mail_address: Option<&'static str>,
}

trait Phone {
	fn new(n: u32, ad: Option<&'static str>) -> Self;
	fn call(&self, n: u32);
	fn send_mail(&self, to: &'static str);
}

impl Phone for classic_Phone {
	fn new(n: u32, ad: Option<&'static str>) -> classic_Phone {
		classic_Phone{
			number: n,
			mail_address: ad,
		}
	}

	fn call(&self, n: u32) {
		println!("this is {}, calling to {} ...", self.number, n);
	}

	fn send_mail(&self, to: &'static str) {
		match self.mail_address {
			Some(s) => {
				println!("{}: Send mail to {}", s, to);
			},
			None => {
				println!("You didn't have mail address");
			},
		}
	}
}

fn main() {
	let myphone = classic_Phone::new(110, None);
	myphone.call(119);
	myphone.send_mail("gmail");
}

