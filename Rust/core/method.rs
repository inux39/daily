
struct A {
	a: i32,
	b: i32,
}

impl A {
	pub fn new(a: i32, b: i32) -> A {
		A {
			a: a,
			b: b,
		}
	}
	pub fn add(&self) -> i32 {
		self.a + self.b
	}
}

fn main() {
	let a = A::new(1, 1);
	println!("{}", a.add());
}

