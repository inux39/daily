
//! Documentation はこんな感じに書ける。
//! さらにMarkdownでいい感じに書くこともできる。
//! 関数の上に書くことで関数の詳細だったり、機能を書くことができる。
//!
//! `//!`は全体の説明

/// 関数の上に書いたものは関数の説明になる
fn main() {
	documentation::test();
}
mod documentation {
	/// テスト
	pub fn test() {
		println!("TEST");
	}
}

