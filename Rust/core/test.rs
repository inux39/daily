
fn main() {

}

fn plus10(n: i32) -> i32 {
	n + 10
}

#[cfg(test)]
mod tests {
	#[test]
	fn test() {
		assert_eq!(20, super::plus10(10));
	}
}

