fn main() {
	let mut v: Vec<u8> = Vec::new();
	for i in 0..20 {
		v.push(i);
	}

	sample_a(&v);
	println!("");
	sample_b(&v);
}

fn sample_a(v: &Vec<u8>) {
	for p in v.chunks(4) {
		for i in p.iter() {
			print!("{:02} ", i);
		}
		println!("");
	}
}

fn sample_b(v: &Vec<u8>) {
	for p in v.chunks(4) {
		println!("{:02}, {:02}, {:02}, {:02}", p[0], p[1], p[2], p[3]);
	}
}

