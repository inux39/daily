
fn main() {
	let p = (10, 20);
	match p {
		(x, y) if x == y => println!("{} = {}", x, y),
		(_, y) if y > 1 => println!("floating!"),
		_ => println!("None"),
	}
}

