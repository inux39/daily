
macro_rules! sum {
	($x:expr) => ($x);
	($x:expr, $($y:expr), +) => (
		$x + sum!($($y), +)
	)
}

fn main() {
	/*
	println!("{}", sum(x, y));
	println!("{}", sum(x, y, z));
	*/
	println!("{}", sum!(10));
	println!("{}", sum!(10, 20, 30));
}
/*
fn sum(x: i32, y: i32) -> i32 {
	x + y
}

fn sum(x: i32, y: i32, z: i32) -> i32 {
	x + y + z
}
*/

