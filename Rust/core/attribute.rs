
//! # target os
//! - unix
//! - linux
//! - windows
//! - macos
//! - ios
//!
//! # other
//! - target_pointer_width
//! - target_arch

fn main() {
	os_type();
	if cfg!(target_os = "linux") {
		println!("I LOVE PENGUIN!");
	}
}

#[allow(dead_code)]
fn unused_fn() {
	println!("dead code");
}

#[cfg(target_os = "linux")]
fn os_type() {
	println!("Hello, Linux!");
}

#[cfg(not(target_os = "linux"))]
fn os_type() {
	println!("Hello, Not Linux!");
}

