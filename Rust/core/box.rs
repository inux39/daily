//! # Box
//! ## メモリの種類
//! 1. プログラム領域(テキスト領域)
//!		コンパイルされたプログラムが格納される。
//! 2. 静的領域(データ領域)
//!		(初期化された)グローバル変数、static変数、Rustの文字列リテラル
//!		などの固定サイズのもの。
//! 3. ヒープ領域
//!		動的なものが格納される。
//! 	RustではVecがヒープに確保される。
//!		Box<T>で値をボックス化、ヒープに割り当てることができる。
//!		だいたい空いているメモリで、32bitだと最大2GBまでらしい。
//! 4. スタック領域
//!		関数の引数、ローカル変数。
//!		OSに依存しない？開発環境によって決まる？
//!		最大値が決まってて多すぎると彼の有名なスタックオーバーフロー。
//! 	Rustでは全ての値はデフォルトでスタックに割り当てられる。
//!
//! ## 速度が速い順
//! 1. 確保しない
//! 1. スタック
//! 1. ヒープ
//!
//! ## Good Document
//! [Stack and Heap](https://rust-lang-ja.github.io/the-rust-programming-language-ja/1.6/book/the-stack-and-the-heap.html)

use std::mem;

#[allow(dead_code)]
struct Yamato {
	weight: i64,
	price: u64,
}

fn packaging() -> Yamato {
	Yamato {
		weight: 500,
		price: 420,
	}
}

fn main() {
	let boxed = Box::new(packaging());
	// ポインタのサイズと同じになる
	println!("Boxed Heap size {} bytes", mem::size_of_val(&boxed));

	let law = packaging();
	// 構造体そのもののデータサイズ
	println!("Stack size {} bytes", mem::size_of_val(&law));

	let unbox = *boxed;
	// boxからコピーしてきたので構造体のサイズ
	println!("Unboxed Stack size {} bytes", mem::size_of_val(&unbox));
}

