
fn main() {
	let mut v: Vec<Vec<i32>> = Vec::new();
	for i in 0..10 {
		v.push(Vec::new());
		for j in 0..10 {
			v[i].push(j as i32);
		}
	}

	for i in &v {
		for j in i {
			print!("{}", j);
		}
		println!("");
	}
}

