use std::borrow::Cow;

fn main() {
	println!("{}", hoge(1));
	println!("{}", hoge(-1));
//	println!("{}", hoge2(1));
}

fn hoge(i: i32) -> Cow<'static, str>{
	if i > 0 {
		"hoge".into()
	} else {
		i.to_string().into()
	}
}

/*
fn hoge2(i: i32) -> str {
	if i > 0 {
		"hoge"
	} else {
		&i.to_string()
	}
}
*/

