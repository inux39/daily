
fn main() {
	function();
	module::public_fn();
	module::call_module();
//	module::private_fn();
}

fn function() {
	println!("Normal function!");
}

mod module {
	fn private_fn() {
		println!("Private function!");
	}
	pub fn public_fn() {
		println!("Public function!");
	}
	mod private_module {
		pub fn private_public_fn() {
			println!("Private Module");
		}
	}
	pub fn call_module() {
		private_module::private_public_fn();
	}
}

