#![allow(unreachable_code)]

fn main() {
	'out: loop {
		println!("Out loop");
		'inner: loop {
			println!("in loop");
			// 内側のループだけを抜ける
			// break;

			// 外のループを抜ける
			break 'out;
		}
		println!("neve reache");
	}
	println!("exit out loop");
}

