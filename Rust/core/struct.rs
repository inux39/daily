#[derive(Debug)]
enum PC_Type {
	Desktop, Laptop, MobilePhone,
}
#[derive(Debug)]
struct PC {
	name: &'static str,
	types: PC_Type,
}

fn main() {
	let pc = PC { name: "MainPC", types: PC_Type::Desktop };
	println!("{} is {}", pc.name, match pc.types {
		PC_Type::Desktop => "Big Black Box",
		PC_Type::Laptop => "ThinkPad",
		PC_Type::MobilePhone => "Android Phone",
	});
	println!("{:?}", pc);
}

