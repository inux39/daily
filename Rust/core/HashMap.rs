
use std::collections::HashMap;
fn main() {
	let mut map = HashMap::new();
	// HashMap<&str, &str> sample
	map.insert("inux39", "test");
	map.insert("hokahoka", "dondon");
	// match key
	match map.get("inux39") {
		Some(k) => println!("{}", k),
		None => println!("not found"),
	}
	match map.get("HokaBen") {
		Some(k) => println!("{}", k),
		None => println!("not found"),
	}
	println!("");
	// show all
	for (a, b) in &map {
		println!("{}/{}", a, b);
	}
	// HashMap Size
	println!("HashMap's size is {} bytes.", std::mem::size_of_val(&map));
	let boxed = Box::new(map);
	println!("Box size is {} bytes.", std::mem::size_of_val(&boxed));
}

