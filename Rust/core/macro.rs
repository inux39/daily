//! [マクロクラブ Rust支部 | κeenのHappy Hacκing Blog](https://keens.github.io/blog/2018/02/17/makurokurabu_rustshibu/)

macro_rules! double_println {
	($ex: expr) => (
		println!("{}{}", $ex, $ex);
	// セミコロンなしでも可
	);
}

macro_rules! simple_macro {
	// `()` はマクロが引数をとらないことを示す
	() => (
		println!("Hello Macro!");
	);
}

macro_rules! name {
	// expr: 式文
	($x: expr) => (
		println!("{}", $x);
	// 複数書く場合はセミコロン必須
	// 忘れないように書いててもいいっぽい
	);
	($x: expr, $($y: expr), +) => (
		println!("{}", $x);
		name!($($y), +)
	);
}

macro_rules! fnction {
	// ident: 関数・変数名に使用する
	($fn_name: ident) => (
		fn $fn_name() {
			// stringify! はidentを文字列に そのまま 変換する
			println!("called {}", stringify!($fn_name));
		}
	)
}

fnction!(test);
fn main() {
	double_println!("double");
	simple_macro!();
	name!("Susumu", "Hirasawa");
	test();
}

