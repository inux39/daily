
fn main() {
	let v: [&'static str; 3] = ["stand", "alone", "complex"];
	show(&v);
	println!("{}", v.len());
}

fn show(s: &[&'static str]) {
	println!("slice length {}", s.len());
}

