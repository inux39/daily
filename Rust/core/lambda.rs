
fn main() {
	fn function(i: i32) -> i32 { i + 1 }
	let closure = | i | i + 1;
	let closure2 = | i: i32 | -> i32 { i+ 1 };
	let i = 1;
	println!("function {}", function(i));
	println!("closure  {}", closure(i));
	println!("closure2 {}", closure2(i));

	let c = | i: i32 | -> bool {
		let mut n = i;
		n += 100;
		n > 200
	};
	println!("c {}", c(i));
}

