
fn main() {
	let age = 15;
	// match内では@をつかって変数をバインディングすることができる
	match age {
		0 => println!("ogya-"),

		// 1 ~ 12 の値を一度にmatchさせることができる
		// マッチした値を`n`にバインディングすることで値を使用できる
		n @ 1 ... 12 => println!("Child of age {}", n),
		n @ 13 ... 19 => println!("Teen of age {}", n),
		n => println!("Age {}", n),
	}
}

