#include <iostream>
#include <memory>

int main(void) {
    std::unique_ptr<int> ptr(new int(10));

    *ptr = 20;

    std::cout << *ptr << std::endl;
    return 0;
}

