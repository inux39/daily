
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let n: usize = s.trim().parse().unwrap();
	let mut max = -1;
	let mut min = 10;
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let v: Vec<&str> = s.trim().split(' ').collect();
	for i in 0..n {
		let n = v[i].parse().unwrap();
		if i == 0 {
			max = n;
			min = n;
		} else {
			if n > max {
				max = n;
			}
			if n < min {
				min = n;
			}
		}
	}
	println!("{}", if max - min > 0 {
		max - min
	} else {
		(max - min) * - 1
	});
}

