
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let n: i64 = s.trim().parse().unwrap();
	let mut x: i64 = n;
	loop {
		if x % 2 == 0 && x % n == 0 {
			println!("{}", x);
			break;
		}
		x *= 2;
	}
}

