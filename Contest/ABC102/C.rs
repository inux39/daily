fn main() {
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let n: usize = s.trim().parse().unwrap();
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let mut v: Vec<i64> = Vec::new();
	for i in 0..vec.len() {
		v.push(vec[i].parse().unwrap());
	}
	let mut B: Vec<i64> = Vec::new();
	for i in 0..n {
		B.push(v[i] - i as i64);
	}
	B.sort();
	let mut min: i64 = 10000;
	for i in 0..1000000 {
		for j in &B {
			let x = abs(j - i);
			if x < min {
				min = x;
				println!("{}", x );
			}
		}
	}
}

/*
fn exec(vec: &Vec<i64>, b: usize) -> (usize, i64){
	let mut wx = 0;
	for i in 0..vec.len() {
		let vn: i64 = vec[i];
		wx += abs(vn - (i + 1) as i64);
	}
	(b, wx)
}
*/

fn abs(n: i64) -> i64 {
	if n < 0 {
		n * - 1
	} else {
		n
	}
}

