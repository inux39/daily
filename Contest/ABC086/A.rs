
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let a = vec[0].parse::<i32>().unwrap();
	let b = vec[1].parse::<i32>().unwrap();
	if (a * b) % 2 == 1 {
		println!("Odd");
	} else {
		println!("Even");
	}
}

