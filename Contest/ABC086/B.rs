
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let mut n = String::new();
	for v in &vec {
		n += &format!("{}", v);
	}
	let p = n.parse::<i32>().unwrap();
	for i in 0..10000 {
		let s = i * i;
		if s == p {
			println!("Yes");
			return;
		}
	}
	println!("No");
}

