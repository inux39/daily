
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let _h: usize = vec[0].parse().unwrap();
	let _w: usize = vec[1].parse().unwrap();
	let mut campus: Vec<Vec<String>> = Vec::new();

	for __h in 0.._w {
		let mut s = String::new();
		std::io::stdin().read_line(&mut s).unwrap();
		let mut x: Vec<String> = Vec::new();
		for j in s.trim().chars() {
			x.push(j.to_string().clone());
		}
		campus.push(x.clone());
	}


	let mut _g = false;

	for h in 0.._h {
		for w in 0.._w {
			if campus[h][w] != "#" {
				break;
			}
			if w < _w - 1 {
				if campus[h][w] == campus[h][w + 1] {
					_g = true;
				}
			}
			if h < _h - 1 {
				if campus[h][w] == campus[h + 1][w] {
					_g = true;
				}
			}
		}
	}

	println!("{}", if _g { "Yes" } else { "No" });
}

