
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let mut v: Vec<i32> = Vec::new();
	for x in &vec {
		v.push(x.parse().unwrap());
	}
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let k: usize = s.trim().parse().unwrap();
	v.sort_by(|a, b| b.cmp(a));
	for _i in 0..k {
		v[0] *= 2;
	}
	let mut n = 0;
	for i in 0..v.len() {
		n += v[i];
	}
	println!("{}", n);
}

