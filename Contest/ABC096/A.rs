
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let nm: i32 = vec[0].parse().unwrap();
	let nd: i32 = vec[1].parse().unwrap();
	let c = if nm <= nd {
		nm
	} else {
		nm - 1
	};
	println!("{}", c);
}

