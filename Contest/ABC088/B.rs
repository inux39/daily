
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let vec: Vec<&str> = s.trim().split(' ').collect();
	let mut a: Vec<i32> = Vec::new();
	for v in &vec {
		a.push(v.parse().unwrap());
	}
	a.sort();
	a.reverse();
	let mut alice = 0;
	let mut bob = 0;
	let mut ab = false;
	for i in &a {
		if !ab {
			alice += *i;
		} else {
			bob += *i;
		}
		ab = !ab;
	}
	println!("{}", alice - bob);
}

