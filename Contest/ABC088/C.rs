
fn main(){
	let mut v: Vec<Vec<i32>> = Vec::new();
	for i in 0..3 {
		v.push(Vec::new());
		let mut s = String::new();
		std::io::stdin().read_line(&mut s).unwrap();
		let vec: Vec<&str> = s.trim().split(' ').collect();
		for j in &vec {
			v[i].push(j.parse().unwrap());
		}
	}
	let mut flag = false;
	for i in 0..8 {
		let mut n = 0;
		for j in 0..3 {
			match i {
				0 | 1 | 2 => {
					n += v[0][j];
				},
				3 | 4 | 5 => {
					n += v[j][0];
				},
				6 => {
					n += v[j][j];
				},
				7 => {
					n += v[match j {
						0 => 2, 2 => 0, _ => 1,
					}][j];
				},
				_ => {},
			};
		}
		for y in &v {
			for x in y {
				if *x == n {
					flag = true;
				}
			}
		}
	}

	println!("{}", if flag { "Yes" } else { "No" });
}

