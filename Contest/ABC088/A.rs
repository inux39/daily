
fn main(){
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let n = s.trim().parse::<i32>().unwrap();
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let a = s.trim().parse::<i32>().unwrap();
	let ten = ((n / 10) % 10) * 10 + (n % 10);
	let hund = ((n / 100) % 10) * 100 + ((n / 10) % 10) * 10 + (n % 10);
	let hyaku = ((n / 100) % 10) > 5;
	if ((n / 100) % 100) % 5 == 0 {
		println!("{}", if a - ten >= 0 { "Yes" } else { "No" });
	} else if hyaku {
		println!("{}", if a - (hund - 500) >= 0 { "Yes" } else { "No" });
	} else {
		println!("{}", if a - hund >= 0 { "Yes" } else { "No" });
	}
}

