// visible class
package foo
private fun foo() {}	// inside this kt
public var bar: Int = 5	// everywhere
private set	// setter only in this kt
internal var baz = 6	// inside the same module

