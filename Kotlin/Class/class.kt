// Class
fun main(args: Array<String>) {
	var c = Constructors(10);
}

class Constructors {
	init {
		println("Init block");
	}

	constructor(n: Int) {
		println("Constructor");
	}
}

