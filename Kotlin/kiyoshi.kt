import java.util.Random;
// false: zun, true: doko
fun main(args: Array<String>) {
	var c = 0;
	while(true) {
		var r = Math.abs(Random().nextInt()) % 2 == 0;
		if(r) {
			print("ドコ ");
		} else {
			print("ズン ");
		}
		if(c == 4 && r) {
			println("キ・ヨ・シ!")
			break;
		}
		if(!r) {
			c += 1;
		} else {
			c = 0;
		}
	}
}

