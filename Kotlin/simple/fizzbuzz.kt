fun main(args: Array<String>) {
	for (i in 1..100) {
		println("${fizzbuzz(i)}");
	}
}

fun fizzbuzz(i: Int): String {
	var s = String();
	if (i % 3 == 0) {
		s += "Fizz";
	}
	if (i % 5 == 0) {
		s += "Buzz";
	}
	if (s.isEmpty()) {
		s += i;
	}
	return s;
}

