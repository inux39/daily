// optional package header
// package sample

fun main(args: Array<String>) {
	println("Hello World!")
	// ;
	// option! semicolon
}

// compile
// ```
// kotlinc source.kt -include-runtime -d tea.jar
// ```

