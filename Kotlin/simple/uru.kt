fun main(args: Array<String>) {
	test_is_uru();
}

fun is_uru(y: Int): Boolean {
	if (y % 4 == 0) {
		if (y % 100 == 0 && y % 400 != 0) {
			return false;
		} else {
			return true;
		}
	} else {
		return false;
	}
}

fun to_string(b: Boolean): String {
	if (b) {
		return "うるう年";
	} else {
		return "うるう年でない";
	}
}

fun test_is_uru() {
	assert(is_uru(2000) == true);
	assert(is_uru(2500) == false);
}

