// variables
fun main(args: Array<String>) {
	val x = 0;	// immutable
//	x = 10;		// <~ Error!
	var y = 0;	// mutable
	y = 10;		// <- OK!
	println("${x}");
	println("${y}");

	val ichiman = 10_000;	// Int
	val number = 1234_5678_9000L;	// Long
	val hex = 0xFF;
	val bytes = 0b10110010;

}

// Type: Bit
// =========
// Long: 64
// Int: 32
// Short: 16
// Byte: 8
// Double: 64
// Float: 32

