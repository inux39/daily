// Control Flow
fun main(args: Array<String>) {
	val x = 10;
	/*
	match x {
		1 => println!("x is one"),
		2 => println!("x is two"),
		_ => println!("x is something"),
	}
	*/
	when (x) {
		1 -> println("x is one");
		2 -> println("x is two");
		else -> {
			println("x is something");
		}
	}
}

