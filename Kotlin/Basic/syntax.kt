// [Basic Syntax - Kotlin Programming Language](https://kotlinlang.org/docs/reference/basic-syntax.html)
// main関数の引数はこれ
// fun main(args: Array<String>) {
fun main(args: Array<String>) {
	// 型を指定する
	var x: Int = 10;
	// 型を推定する
	var y = 20;
	println("${sum(10, 20)}");
	// if () である必要がある
	if (x + y == sum(x, y)) {
		println("Success: ${sum(x, y)}");
	}
	// Ok!
	x = 30;
	// {} はなくてもかまわない
	if (x + y == sum(x, y)) println("Success: ${sum(x, y)}");
	/*
	   TODO
	// for (var in iterator)
	for (i in 10) {
		println("${i}");
	}
	*/
}

fun twice(a: Int) = a * 2

// こういうのはできないっぽい
/*
fun twice2(a: Int) = {
	a * 2
}
*/

fun sum(x: Int, y: Int): Int {
	return x + y;
//	returnは必須
// 	x + y
}

// Unitはなにもないことを表すヤツ
// 書かなくてもいい
fun print_num(n: Int): Unit {
	println("${n}")
}

