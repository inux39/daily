mod error;
use clap::{Arg, App};
use self::error::Result;

const SEASON: [&'static str; 5] = ["春", "夏", "秋", "冬", "年末"];

fn main() {
    let cmd = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .version_short("v")
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(Arg::with_name("parent")
            .takes_value(true)
            .required(true)
            .multiple(false)
            .help("Set parent directory"))
        .arg(Arg::with_name("year")
            .short("y")
            .long("year")
            .help("Create directories by year"))
        .arg(Arg::with_name("new")
            .short("n")
            .long("new")
            .help("Create directories by production"))
        .get_matches();

    let parent = cmd.value_of("parent").unwrap();
    if cmd.is_present("new") {
        mkdir_by_production(parent.to_string()).unwrap();
    }

    if cmd.is_present("year") {
        mkdir_by_year(parent.to_string()).unwrap();
    }
}

fn mkdir_by_year(dir: String) -> Result<()> {

    Ok(())
}

fn mkdir_by_production(dir: String) -> Result<()> {
    let root = std::path::Path::new(&dir);
    let current = std::env::current_dir()?;
    for entry in std::fs::read_dir(&current)? {
        let file = match entry?.file_name().into_string() {
            Ok(f) => f,
            Err(_) => continue,
        };
        let at = file.rfind(".webm");
        if at == None {
            continue;
        }
        let (file_name, _) = file.split_at(at.unwrap());

        let at = file_name.rfind("#01");
        if at == None {
            continue;
        }
        let (file_name, _) = file_name.split_at(at.unwrap());
        let path = root.join(file_name.trim());
        std::fs::create_dir(path)?;
    }

    Ok(())
}

