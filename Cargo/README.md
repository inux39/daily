LINK
===
## Document Link
1. GUI
	- [gtk](http://gtk-rs.org/docs/gtk/)
		+ [examples](https://github.com/gtk-rs/examples/)
		+ [Gtk+ 3](https://developer.gnome.org/gtk3/stable/)
	- [gio](http://gtk-rs.org/docs/gio/)
	- [cairo](https://docs.rs/cairo/)

1. Network
	- [hyper](https://docs.rs/hyper/)
		+ [examples](https://hyper.rs/guides/)
	- [reqwest](https://docs.rs/reqwest/)

1. Data
	- [sqlite](https://docs.rs/sqlite/)
	- [serde](https://docs.serde.rs/serde/)
		+ [examples](https://serde.rs/)
	- [image](https://docs.rs/image/)

1. System
	- [time](https://doc.rust-lang.org/time/time/index.html)
	- [pancurses](https://docs.rs/pancurses)
	- [rand](https://docs.rs/rand/)
	- [regex](https://docs.rs/regex/)

1. Graphics
	- [servo-glutin](https://docs.rs/servo-glutin/)
	- [glutin](https://docs.rs/glutin/)
	- [gl](https://docs.rs/gl)
	- [glfw](https://docs.rs/glfw/)
		+ [GLFW Documentation](http://www.glfw.org/docs/latest/)
		+ [GLFWによるOpenGL入門](http://marina.sys.wakayama-u.ac.jp/~tokoi/GLFWdraft.pdf)
		+ [床井研究室 - (1) GLFW で OpenGL を使う](http://marina.sys.wakayama-u.ac.jp/~tokoi/?date=20120906)

