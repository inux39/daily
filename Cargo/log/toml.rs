mod file_io;
use file_io::*;
use std::path::Path;

#[macro_use]
extern crate serde_derive;
extern crate toml;

const FILE: &'static str = "test.toml";

#[derive(Serialize, Deserialize, Debug)]
struct Config {
	window_title: Option<String>,
	window_position: Coordinate,
	window_size: Coordinate,
	// 最後のフィールドはOptionではないものである必要がある
	// panic -> ValueAfterTable
}

#[derive(Serialize, Deserialize, Debug)]
struct Coordinate {
	x: i32,
	y: i32,
}

fn main() {
	let cfg = Config {
		window_position: Coordinate {
			x: 100,
			y: 200,
		},
		window_size: Coordinate {
			x: 250,
			y: 250,
		},
		window_title: None,
	};

	let path = Path::new(FILE);
	let s = toml::to_string(&cfg).unwrap();
	write(path, s).unwrap();

	let toml = read_to_string(path).unwrap();
	println!("{}", toml);
}

