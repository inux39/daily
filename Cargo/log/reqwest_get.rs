extern crate reqwest;
use std::error::Error;
fn main() {
	let mut resp = match reqwest::get("https://www.inux39.me") {
		Ok(t) => t,
		Err(e) => panic!("{}", e.description()),
	};
	let body = match resp.text() {
		Ok(t) => t,
		Err(e) => panic!("{}", e.description()),
	};
	println!("{}", body);
}

