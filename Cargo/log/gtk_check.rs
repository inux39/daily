extern crate gtk;
extern crate gio;

use gtk::prelude::*;
use gio::prelude::*;
use std::env::args;

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

fn main() {
	let app = gtk::Application::new("me.inux39.simple.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize Failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application){
	let window = gtk::ApplicationWindow::new(app);
	let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	let button = gtk::Button::new_with_label("Click!");
	let button_exit = gtk::Button::new_with_label("Exit");
	let check1 = gtk::CheckButton::new_with_label("Check1");
	let check2 = gtk::CheckButton::new_with_label("Check2");
	let check3 = gtk::CheckButton::new_with_label("Check3");
	let label = gtk::Label::new("Check all!");
	// Yoko
	hbox.add(&check1);
	hbox.add(&check2);
	hbox.add(&check3);
	hbox.add(&button);
	// Tate
	vbox.add(&hbox);
	vbox.add(&label);
	vbox.add(&button_exit);
	// Window
	window.set_title("Simple Gtk Clicker!");
	window.set_border_width(10);
	window.set_position(gtk::WindowPosition::Center);
	window.set_default_size(200, 300);
	// Object Config
	button_exit.connect_clicked(clone!(window => move |_| {
		window.destroy();
	}));
	   // All check button were clicked, label set new string
	check1.connect_clicked(clone!(label => move |_| {
		label.set_text("check1 checked!");
	}));
	check2.connect_clicked(clone!(label => move |_| {
		label.set_text("check2 checked!");
	}));
	check3.connect_clicked(clone!(label => move |_| {
		label.set_text("check3 checked!");
	}));
	window.add(&vbox);
	window.show_all();
	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));
}

