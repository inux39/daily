extern crate gtk;
extern crate gio;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use gtk::prelude::*;
use gio::prelude::*;
use std::env::args;
use std::error::Error;

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

/// Request Generator
/// Request and Response are UTF-8
struct Params {
	/// xxx-xxxx or xxxxxxx(7 letters)
	zipcode: String,
	/// "ja" or "roma": Japanese or Japanese roman letter.
	/// Default: "ja"
	lang: Option<String>,
	/// JSONP Request parameters.
	callback: Option<String>,
}

impl Params {
	pub fn new(z: String, l: Option<String>, c: Option<String>) -> Params {
		Params {
			zipcode: z,
			lang: l,
			callback: c,
		}
	}

	fn create_params(self) -> String {
		let mut n = String::new();
		n += &format!("?zipcode={}", self.zipcode);
		self.lang.map(|lang| n += &format!("&lang={}", lang));
		self.callback.map(|cb| n += &format!("&callback={}", cb));
		n
	}

	pub fn get(self) -> String {
		let url = format!("http://api.zipaddress.net/{}", self.create_params());
		let mut resp = match reqwest::get(&url) {
			Ok(t) => t,
			Err(e) => panic!("{}", e.description()),
		};
		match resp.text() {
			Ok(t) => t,
			Err(e) => panic!("{}", e.description()),
		}
	}
}

// Response
#[derive(Serialize, Deserialize, Debug)]
struct ZipAddress {
	/// 住所が引けた場合200
	code: u32,
	/// zip codeが存在しており、住所がある場合に帰ってくる
	data: Option<Data>,
	/// code が200以外のときに含まれるはず
	message: Option<String>,
}

// レスポンスがUTF-8で帰ってくる(たしかにUTF-8)
// UTF-16エンコードのものがUTF-8エンコードされてくる
#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
struct Data {
	pref: String,
	address: String,
	city: String,
	town: String,
	fullAddress: String,
}

fn main() {
	let app = gtk::Application::new("me.inux39.simple.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize Failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application){
	let window = gtk::ApplicationWindow::new(app);

	window.set_title("Simple Zip Address Translation");
	window.set_border_width(10);
	window.set_position(gtk::WindowPosition::Center);
	window.set_default_size(200, 200);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	let button = gtk::Button::new_with_label("Translate!");
	let exit_button = gtk::Button::new_with_label("Exit");
	let app_label = gtk::Label::new("ZIP code here!");
	let label = gtk::Label::new("Push Translate button! will appear here.");
	let text_view = gtk::TextView::new();
	vbox.add(&app_label);
	vbox.add(&text_view);
	vbox.add(&label);
	vbox.add(&button);
	vbox.add(&exit_button);
	window.add(&vbox);
	button.connect_clicked(move |_| {
		let mut s = String::new();
		text_view.get_buffer().map(|buf| {
			buf.get_text(&buf.get_start_iter(), &buf.get_end_iter(), true).map(|n| s = n)
		});
		let zip = Params::new(s, None, None);
		let n = zip.get();
		let p: ZipAddress = serde_json::from_str(&n).unwrap();
		p.data.map(|d| label.set_text(&format!("Full Address is {}", d.fullAddress)));
		p.message.map(|m| label.set_text(&m));
	});
	exit_button.connect_clicked(clone!(window => move |_| {
		window.destroy();
	}));
	window.show_all();
	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));
}

