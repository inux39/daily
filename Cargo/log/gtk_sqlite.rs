extern crate gtk;
extern crate gio;
extern crate sqlite;

use gtk::prelude::*;
use gio::prelude::*;
use std::env::args;

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

fn main() {
	let app = gtk::Application::new("me.inux39.simple.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize Failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application){
	let window = gtk::ApplicationWindow::new(app);
	let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	let text = gtk::TextView::new();
	let label = gtk::Label::new(None);
	let button_add = gtk::Button::new_with_label("Add");
	let button_exit = gtk::Button::new_with_label("Exit");
	button_exit.connect_clicked(clone!(window => move |_| {
		window.destroy();
	}));
	button_add.connect_clicked(clone!(label => move |_| {
		label.set_text(&format!("{:?}", read()));
	}));
	// Yoko
	hbox.add(&text);
	hbox.add(&button_add);
	// Tate
	vbox.add(&hbox);
	vbox.add(&button_exit);
	vbox.add(&label);
	// Window
	window.set_title("Simple Gtk and SQLite!");
	window.set_border_width(10);
	window.set_position(gtk::WindowPosition::Center);
	window.set_default_size(200, 300);
	window.add(&vbox);
	window.show_all();
	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));
}

fn write(id: i32, n: &str) {
	let connection = sqlite::open(":memory:").unwrap();
	connection.execute(
		"
		CREATE TABLE items (id INTEGER, name TEXT);
		"
	).unwrap();
	connection.execute(
		format!("INSERT INTO items (id, name) VALUES ('{}', '{}');",
			id, n)
	).unwrap();
}

fn read() -> (){
	let connection = sqlite::open(":memory:").unwrap();
	connection.iterate("SELECT * FROM items WHERE id >= 0", |p| {

	}).unwrap()
}

