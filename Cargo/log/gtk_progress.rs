extern crate gtk;
extern crate gio;

use gtk::prelude::*;
use gio::prelude::*;
use std::env::args;

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

fn main() {
	let app = gtk::Application::new("me.inux39.simple.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize Failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application){
	let window = gtk::ApplicationWindow::new(app);
	let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	let progress = gtk::ProgressBar::new();
	let button_add = gtk::Button::new_with_label("Click!");
	let button_exit = gtk::Button::new_with_label("Exit");
	// Yoko
	hbox.add(&progress);
	hbox.add(&button_add);
	// Tate
	vbox.add(&hbox);
	vbox.add(&button_exit);
	// Window
	window.set_title("Simple Gtk Clicker!");
	window.set_border_width(10);
	window.set_position(gtk::WindowPosition::Center);
	window.set_default_size(200, 300);
	// Object Config
	button_exit.connect_clicked(clone!(window => move |_| {
		window.destroy();
	}));
	button_add.connect_clicked(move |_| {
		progress.set_fraction(progress.get_fraction() + 0.05);
		if progress.get_fraction() >= 1.0 {
			progress.set_fraction(0.0);
		}
	});
	window.add(&vbox);
	window.show_all();
	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));
}

