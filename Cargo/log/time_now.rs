extern crate time;

fn main() {
	let jst_now = time::now();
	let utc_now = time::now_utc();
	println!("{}", jst_now.rfc822());
	println!("{}", utc_now.rfc822());
	match jst_now.strftime("%Y年%m月%d日(%a) %H時%M分%S秒") {
		Ok(t) => println!("{}", t),
		Err(_) => {},
	};
	let acc = 100.0f32;
	let per = jst_now.tm_yday as f32 / 365.0f32 * acc;
	let round = (per * acc).round();
	println!("{}%経過しました。",   round / acc);
	println!("あと{}%です。", 100.0f32 - (round / acc));
}

