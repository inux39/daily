extern crate glfw;
extern crate gl;
use glfw::{Action, Context, Key};
use gl::types::*;
fn main() {
	let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

	let (mut window, events) = glfw.create_window(300, 300,
		"hello this is window", glfw::WindowMode::Windowed)
		.expect("Failed to create GLFW window.");

	window.make_current();
	window.set_key_polling(true);
	unsafe {
		gl::Clear(gl::COLOR_BUFFER_BIT);
		gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);
		gl::ClearColor(0.0, 0.0, 0.0, 0.0);
	};

	while !window.should_close() {
		unsafe {
			gl::Clear(gl::COLOR_BUFFER_BIT);
		};

		glfw.poll_events();
		for (_, event) in glfw::flush_messages(&events) {
			println!("{:?}", event);
			match event {
				glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) =>{
					window.set_should_close(true)
				},
				glfw::WindowEvent::Key(Key::Enter, _, Action::Press, _) |
				glfw::WindowEvent::Key(Key::Enter, _, Action::Repeat, _) => {
					unsafe {
						gl::ClearColor(0.0, 0.0, 0.0, 0.0);
					};
				},
				_ => {},
			}
		}
		window.swap_buffers();
	}
}

