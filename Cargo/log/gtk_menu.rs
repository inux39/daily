
extern crate gtk;
extern crate gio;
use std::env::args;
use gio::{ApplicationExt, ApplicationExtManual};
use gtk::prelude::*;
use gtk::{ApplicationWindow, WindowPosition, Menu, MenuBar, MenuItem, Image};
fn main() {
	let application = gtk::Application::new("me.inux39.menu.gtk",
		gio::ApplicationFlags::empty())
		.expect("failed initialize");
	application.connect_startup(move |app| {
		build_ui(app);
	});
	application.connect_activate(|_| {});
	application.run(&args().collect::<Vec<_>>());
}

fn build_ui(application: &gtk::Application) {
	let window = ApplicationWindow::new(application);

	window.set_title("MenuBar");
	window.set_position(WindowPosition::Center);
	window.set_size_request(400, 400);

	let v_box = gtk::Box::new(gtk::Orientation::Vertical, 10);

	let menu = Menu::new();
	let menu_bar = MenuBar::new();
	let file = MenuItem::new_with_label("File");
	let about = MenuItem::new_with_label("About");
	let quit = MenuItem::new_with_label("Quit");
	let file_item = MenuItem::new();
	let file_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);
	menu.append(&about);
	menu.append(&quit);
	file.set_submenu(Some(&menu));
	menu_bar.append(&file);
	menu_bar.append(&about);
	menu_bar.append(&quit);
	//menu_bar.append(&file);

	v_box.pack_start(&menu_bar, false, false, 0);

	window.add(&v_box);
	window.show_all();
}

