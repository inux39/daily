//! [Copyright (c) 2014 The Rust Project Developers](https://github.com/serde-rs/serde/blob/master/LICENSE-MIT)
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

#[derive(Serialize, Deserialize, Debug)]
struct Point {
	x: i32,
	y: i32,
#[serde(rename = "type")]
	_type: String,
}

fn main() {
	let point = Point { x: 10, y: 20, _type: "Cube".to_string() };
	let serialize = serde_json::to_string(&point).unwrap();
	println!("Serialized: {}", serialize);

	let deserialize: Point = serde_json::from_str(&serialize).unwrap();
	println!("{:?}", deserialize);
}

