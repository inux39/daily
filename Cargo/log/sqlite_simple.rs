extern crate sqlite;

fn main() {
	let connection = sqlite::open(":memory:").unwrap();
	connection.execute(
		"
		CREATE TABLE items (id INTEGER, name TEXT);
		"
	).unwrap();
	let item_list = ["おまもり", "おみくじ", "はまや", "おそなえ"];
	for i in 0..item_list.len() {
		connection.execute(
			format!("INSERT INTO items (id, name) VALUES ('{}', '{}');",
				i, item_list[i])
		).unwrap();
	}

	connection.iterate("SELECT * FROM items WHERE id >= 0", |pairs| {
		/*
		for &(column, value) in pairs.iter() {
			println!("{} = {}", column, value.unwrap());
		}
		*/
		println!("{:?}", pairs);
		true
	}).unwrap();
}

