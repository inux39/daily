extern crate futures;
extern crate hyper;
extern crate hyper_tls;
extern crate tokio_core;

use std::error::Error;
use futures::{Future, Stream};
use hyper::Client;
use hyper_tls::HttpsConnector;
use tokio_core::reactor::Core;

fn main(){
	// connection
	let mut core = Core::new().unwrap();
	let handle = core.handle();
	let client = Client::configure()
		.connector(HttpsConnector::new(4, &handle).unwrap())
		.build(&handle);
	// get!
	let work = client.get("https://www.inux39.me".parse().unwrap())
		.and_then(|res| res.body().concat2());
	let get = core.run(work).unwrap();
	let body = match std::str::from_utf8(&get) {
		Err(why) => panic!("Error {}", why.description()),
		Ok(s) => s,
	};
	println!("{}", body);
}

