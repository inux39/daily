use std::io::{Read, Write};
use std::fs::{File};
use std::path::Path;

#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate gtk;
extern crate gio;
extern crate time;

use gtk::prelude::*;
use gio::prelude::*;
use std::env::args;

const FILE: &'static str = "text.toml";

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

#[derive(Serialize, Deserialize, Debug)]
struct Config {
	window_position: Coordinate,
	window_size: Coordinate,
}

#[derive(Serialize, Deserialize, Debug)]
struct Coordinate {
	x: i32,
	y: i32,
}

fn main() {
	let app = gtk::Application::new("me.inux39.simple.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize Failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application){
	let window = gtk::ApplicationWindow::new(app);
	// Read config
	let file = read();
	let deser: Config = match toml::from_str(&file) {
		Err(_) => Config {
			window_position: Coordinate {
				x: 200,
				y: 200,
			},
			window_size: Coordinate {
				x: 250,
				y: 250,
			},
		},
		Ok(t) => t,
	};
	let (wx, wy) = (deser.window_size.x, deser.window_size.y);
	let (px, py) = (deser.window_position.x, deser.window_position.y);
	window.set_title("Gtk_text");
	window.set_border_width(10);
	window.set_default_size(wx, wy);
	window.move_(px, py);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	let button = gtk::Button::new_with_label("Set window title");
	let exit_button = gtk::Button::new_with_label("Exit");
	let label = gtk::Label::new(None);
	let text_view = gtk::TextView::new();
	let time = gtk::Label::new(None);
	vbox.add(&text_view);
	vbox.add(&label);
	vbox.add(&button);
	vbox.add(&exit_button);
	vbox.add(&time);
	window.add(&vbox);
	button.connect_clicked(clone!(window => move |_| {
		let mut s = String::new();
		text_view.get_buffer().map(|buf| {
			buf.get_text(&buf.get_start_iter(), &buf.get_end_iter(), true).map(|n| s = n)
		});
		window.set_title(&s);
	}));
	exit_button.connect_clicked(clone!(window => move |_| {
		let (posi_x, posi_y) = window.get_position();
		let (size_x, size_y) = window.get_size();
		let save: Config = Config {
			window_position: Coordinate {
				x: posi_x,
				y: posi_y,
			},
			window_size: Coordinate {
				x: size_x,
				y: size_y,
			},
		};
		let ser = toml::to_string(&save).expect("convert error");
		write(ser);
		window.destroy();
	}));
	window.connect_property_is_active_notify(clone!(window => move |_| {
		println!("{}",
			if window.is_active() {
				"Active"
			} else {
				"Not active"
			});
	}));
	window.connect_property_window_position_notify(clone!(window => move |_| {
		println!("Position: {:?}", window.get_position());
	}));
	window.connect_property_title_notify(clone!(window => move |_| {
		println!("Title changed: {}", match window.get_title() {
			Some(t) => t,
			None	=> "No title.".to_string(),
		});
	}));
	let tick = move || {
		time.set_text(&current_time());
		gtk::Continue(true)
	};
	gtk::timeout_add_seconds(1, tick);
	window.connect_delete_event(clone!(window => move |_, _| {
		let (posi_x, posi_y) = window.get_position();
		let (size_x, size_y) = window.get_size();
		let save: Config = Config {
			window_position: Coordinate {
				x: posi_x,
				y: posi_y,
			},
			window_size: Coordinate {
				x: size_x,
				y: size_y,
			},
		};
		let ser = toml::to_string(&save).expect("convert error");
		write(ser);
		window.destroy();
		Inhibit(false)
	}));
	window.show_all();
}

fn current_time() -> String {
	match time::now().strftime("%Y/%m/%d %H:%M.%S") {
		Ok(t) => t.to_string(),
		Err(_) => "".to_string(),
	}
}

fn read() -> String {
	let path = Path::new(FILE);
	let mut buf = String::new();
	let mut file = match File::open(&path) {
		Err(_) => return buf,
		Ok(f) => f,
	};
	file.read_to_string(&mut buf).unwrap();
	buf
}

fn write(s: String) {
	let path = Path::new(FILE);
	let mut file = match File::create(&path) {
		Ok(f) => f,
		Err(e) => panic!("{:?}", e.kind()),
	};
	match file.write_all(s.as_bytes()) {
		Ok(_) => {},
		Err(e) => panic!("{:?}", e.kind()),
	};
}

