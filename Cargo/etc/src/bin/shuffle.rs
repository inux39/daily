extern crate rand;
use rand::Rng;
fn main() {
	let mut s = String::new();
	std::io::stdin().read_line(&mut s).unwrap();
	let sc: Vec<&str> = s.trim().split_whitespace().collect();
	println!("{}", shuffle(sc));
}

fn shuffle(v: Vec<&str>) -> String {
	let mut s = String::new();
	let mut rng = rand::thread_rng();
	for i in 0..v.len() {
		let mut ss = v[i].chars().collect::<Vec<char>>();
		for _j in 0..ss.len() {
			let rand: usize = rng.gen_range(0, ss.len());
			s += &format!("{}", ss[rand]);
			ss.remove(rand);
		}
	}
	s
}

