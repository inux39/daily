use super::std as std;
use std::io::{Read, Write};
use std::fs::File;
use std::path::Path;

/*
pub fn read_to_u8(path: &Path) -> Result<String, std::io::Error> {
	let mut buf = String::new();
	let mut file = File::open(&path)?;
	file.read_to_string(&mut buf)?;
	Ok(buf)
}
*/

#[allow(dead_code)]
pub fn read_to_string(path: &Path) -> Result<String, std::io::Error> {
	let mut buf = String::new();
	let mut file = File::open(&path)?;
	file.read_to_string(&mut buf)?;
	Ok(buf)
}

#[allow(dead_code)]
pub fn write(path: &Path, s: String) -> Result<(), std::io::Error> {
	let mut file = File::create(&path)?;
	file.write_all(s.as_bytes())
}

#[allow(dead_code)]
pub fn append(path: &Path, s: String) -> Result<(), std::io::Error> {
	let mut file = std::fs::OpenOptions::new()
		.write(true)
		.create(true)
		.append(true)
		.open(&path)?;
	file.write_all(s.as_bytes())
}

