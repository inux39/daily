extern crate regex;
use regex::Regex;

fn main() {
	let r: String = {
		let mut s = String::new();
		std::io::stdin().read_line(&mut s).unwrap();
		s.trim().to_string()
	};
	let reg = Regex::new(&r).unwrap();
	let m: String = {
		let mut s = String::new();
		std::io::stdin().read_line(&mut s).unwrap();
		s.trim().to_string()
	};
	for cap in reg.captures_iter(&m) {
		print!("{:?} ", cap);
	}
}

