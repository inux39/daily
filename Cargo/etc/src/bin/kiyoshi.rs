extern crate rand;
use std::io::{stdout, Write, BufWriter};
// ズン: false, ドコ: true
fn main() {
	let w = stdout();
	let mut w = BufWriter::new(w.lock());
	let mut c = 0;
	loop {
		let r = rand::random();
		write!(w, "{} ", if r { "ドコ" } else { "ズン" }).unwrap();
		if c == 4 && r {
			writeln!(w, "キ・ヨ・シ!").unwrap();
			break;
		}
		c = if !r { c + 1 } else { 0 };
	}
}

