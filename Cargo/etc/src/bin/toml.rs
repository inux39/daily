#[macro_use]
extern crate serde_derive;
extern crate toml;

const FILE: &'static str = "test.toml";

#[derive(Serialize, Deserialize, Debug)]
struct Toml {
	instance: Option<Vec<Instance>>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Instance {
	name: String,
	url: String,
}

fn main() {
	let mut i = Vec::new();
	i.push(new_instance("マストどす".to_string(), "https://mastodos.com".to_string()));
	i.push(new_instance("don.inux39.me".to_string(), "https://don.inux39.me".to_string()));
	i.push(new_instance("mstdn.jp".to_string(), "https://mstdn.jp".to_string()));
	i.push(new_instance("nere9".to_string(), "https://mstdn.nere9.help".to_string()));
	let list = Toml {
		instance: Some(i),
	};
	let s = toml::to_string(&list).unwrap();
	println!("{}", s);
}

fn new_instance(n: String, u: String) -> Instance {
	Instance {
		name: n,
		url: u,
	}
}

