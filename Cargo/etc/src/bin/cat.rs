extern crate regex;
use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::{PathBuf};
use std::env;

#[allow(unused_assignments)]
fn main() {
	let args: Vec<String> = env::args().collect();
	let mut path = PathBuf::new();
	if args.len() < 2 {
		path = PathBuf::from(&args[0]).parent().unwrap().to_path_buf();
	} else {
		path = PathBuf::from(&args[1]);
	}

	path.push("_category.template");
	let mut s = String::new();
	let mut template = match File::open(&path) {
		Err(_) => {
			println!("can't read template file.");
			return;
		},
		Ok(file) => file,
	};
	match template.read_to_string(&mut s) {
		Err(e) => panic!("{}", Error::description(&e)),
		Ok(_) => {}
	};
	path.pop();

	println!("category name:");
	let cat = {
		let mut s = String::new();
		std::io::stdin().read_line(&mut s).unwrap();
		s.trim().to_string()
	};

	let rgx = regex::Regex::new(r"@cat@").unwrap();
	let ccat = rgx.replace_all(&s, cat.as_str());

	path.push(format!("{}.liquid", cat));
	let mut file = match File::create(&path) {
		Err(e) => panic!("{}", Error::description(&e)),
		Ok(file) => file,
	};
	match file.write_all(ccat.as_bytes()) {
		Err(e) => panic!("{}", Error::description(&e)),
		Ok(_) => {
			println!("{:?}", path);
			println!("\"{}\"", ccat);
			println!("Success!");
		},
	}
}

