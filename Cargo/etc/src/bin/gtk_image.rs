extern crate gtk;
extern crate gio;
extern crate gdk;
//extern crate cairo;
extern crate gdk_pixbuf;
use gtk::prelude::*;
use gio::prelude::*;
use gdk::prelude::*;
use gdk_pixbuf::prelude::*;
use std::env::args;

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

fn main() {
	let app = gtk::Application::new("me.inux39.image.gtk",
		gio::ApplicationFlags::empty())
		.expect("Initialize failed...");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application) {
	let window = gtk::ApplicationWindow::new(app);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	window.set_title("GTK Image");
	window.set_border_width(10);
	window.set_default_size(500, 500);

	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));

	let chooser = gtk::FileChooserButton::new(
		"Open File",
		gtk::FileChooserAction::Open);
	let img = gtk::Image::new();
	vbox.add(&chooser);
	vbox.add(&img);

	window.connect_key_press_event(clone!(window => move |_, x| {
		let keyval = x.get_keyval();
		let keyname = gdk::keyval_name(keyval).unwrap();
		println!("{}", keyname);
		if keyname == "Escape" {
			window.destroy();
		}
		Inhibit(false)
	}));

	chooser.connect_selection_changed(clone!(chooser => move |_| {
		let cf = chooser.get_filename().unwrap();
		let pixbuf = gdk_pixbuf::Pixbuf::new_from_file(cf).unwrap();
		let px = pixbuf.scale_simple(100, 100, gdk_pixbuf::InterpType::Hyper);
		img.set_from_pixbuf(Some(&px.unwrap()));
	}));

	window.add(&vbox);
	window.show_all();
}

