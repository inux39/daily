extern crate time;
extern crate rand;
mod file_io;
use std::path::Path;
use rand::Rng;
use file_io::*;

const DEF_WORDS: &'static str = "グエー\nグヘー\nドヒャー\nウオー";
const PATH: &'static str = "ejowords.ejo";

fn main() {
	let fs = match read_to_string(Path::new(PATH)) {
		Ok(s) => s,
		Err(_) => DEF_WORDS.to_string()
	};
	let mut rng = rand::thread_rng();
	let ejoword: Vec<_> = fs.split('\n').collect();
	let rand: usize = rng.gen_range(0, ejoword.len());
	println!("{}", ejonecho(ejoword[rand], false));
	println!("{}", ejonecho(ejoword[rand], true));
}

fn ejonecho(s: &str, ampm: bool) -> String {
	let now = time::now();
	let ejotime = ejoneco_time(now.tm_hour, now.tm_min);
	let han = if is_half(now.tm_min) { "半" } else { "" };

	if ampm {
		let (ejotime, _is_pm) = pm_time(ejotime);
		format!(":ejoneco: < {}{}時{}", s, ejotime, han)
	} else {
		format!(":ejoneco: < {}{}時{}", s, ejotime, han)
	}
}

// だいたい半かな？
fn is_half(m: i32) -> bool {
	if m >= 20 && m <= 40 {
		true
	} else {
		false
	}
}

// だいたい時間をあれする
fn ejoneco_time(h: i32, m: i32) -> i32 {
	if m + 20 > 60 {
		h + 1
	} else {
		h
	}
}

fn pm_time(t: i32) -> (i32, bool) {
	if t > 12 {
		(t - 12, true)
	} else if t == 12 {
		(12, true)
	} else if t == 0 {
		(12, false)
	} else {
		(t, false)
	}
}

#[test]
fn test_ejotime() {
	assert!(ejoneco_time(0, 0) == 0);
	assert!(ejoneco_time(0, 40) == 0);
	assert!(ejoneco_time(0, 41) == 1);
	assert!(ejoneco_time(12, 50) == 13);
	assert!(ejoneco_time(24, 55) == 25);
}

#[test]
fn test_is_half() {
	assert!(is_half(19) == false);
	assert!(is_half(20) == true);
	assert!(is_half(30) == true);
	assert!(is_half(40) == true);
	assert!(is_half(41) == false);
}

#[test]
fn test_pm_time() {
	assert!(pm_time(12) == (12, true));
	assert!(pm_time(0) == (12, false));
	assert!(pm_time(15) == (3, true));
	assert!(pm_time(1) == (1, false));
}

#[test]
fn test_random() {
	let mut t: Vec<bool> = Vec::new();
	for _n in 0..1000 {
		t.push(false);
	}
	let mut rng = rand::thread_rng();
	for _c in 0..(t.len() * 10) {
		let rand: usize = rng.gen_range(0, t.len());
		t[rand] = true;
	}
	let mut c = 0;
	for i in &t {
		if !i {
			c += 1;
		}
	}
	assert!(c == 0);
}

