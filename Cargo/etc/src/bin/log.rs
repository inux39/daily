extern crate chrono;
extern crate regex;
extern crate gtk;
extern crate gdk;
extern crate gio;
use std::io::{BufReader, BufRead};
use std::fs::File;
use std::env::args;
use regex::Regex;
use gtk::prelude::*;
use gio::prelude::*;
use chrono::prelude::*;
use chrono::Duration;

const DEBUG: bool = false;
//const FILE_CONFIG: &'static str = "config.toml";
const REG_TITLE: &'static str = "\\.ts";
const REG_TIME: &'static str = "(\\d{4}.[ \\t\\n\\r\\f\\v]+\\d++.[ \\t\\n\\r\\f\\v]+\\d++.[ \\t\\n\\r\\f\\v]+\\S+[ \\t\\n\\r\\f\\v]+\\d{2}:\\d{2}:\\d{2}[ \\t\\n\\r\\f\\v]+\\S[^,]+)";

macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => ({
		$( let $n = $n.clone(); )+
		move || $body
	});
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => ({
		$( let $n = $n.clone(); )+
		move |$(clone!(@param $p),)+| $body
	});
}

fn main() {
	let app = gtk::Application::new("me.inux39.log",
		gio::ApplicationFlags::empty())
		.expect("Failed to initialize");
	app.connect_startup(|app| {
		build_ui(app);
	});
	app.connect_activate(|_| {});
	app.run(&args().collect::<Vec<_>>());
}

fn build_ui(app: &gtk::Application) {
	let window = gtk::ApplicationWindow::new(app);
	let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
	window.set_title("Log");
	window.set_border_width(10);
	window.set_default_size(1000, 600);
	window.connect_delete_event(clone!(window => move |_, _| {
		window.destroy();
		Inhibit(false)
	}));

	let chooser = gtk::FileChooserButton::new(
		"OpenFile",
		gtk::FileChooserAction::Open);

	// スクロールできるようにするやつ
	let scrolled = gtk::ScrolledWindow::new(None, None);
	let treeview = gtk::TreeView::new();
	treeview.set_headers_visible(true);
	// スクロールできるようにするやつにTreeViewを入れる
	scrolled.add(&treeview);

	let column_types = [gtk::Type::String, gtk::Type::String, gtk::Type::String, gtk::Type::String];
	let tree_store = gtk::ListStore::new(&column_types);

	// ファイル名
	append_column(&treeview, 0);
	// 開始時刻
	append_column(&treeview, 1);
	// 終了時刻
	append_column(&treeview, 2);
	// 経過時間
	append_column(&treeview, 3);
	treeview.set_model(Some(&tree_store));

	// show boxes
	vbox.pack_start(&chooser, false, false, 0);
	vbox.pack_start(&scrolled, true, true, 5);
	window.add(&vbox);

	chooser.connect_selection_changed(move |chooser| {
		// ここですべての処理をさせる
		let name = chooser.get_filename().unwrap();
		let log = read_log(name);
		let parsed = parse_log(log);
		for i in 0..parsed[0].len() {
			let iter = tree_store.append();
			tree_store.set_value(&iter, 0, &parsed[0][i].to_value() as &gtk::Value);
			tree_store.set_value(&iter, 1, &parsed[1][i].to_value() as &gtk::Value);
			tree_store.set_value(&iter, 2, &parsed[2][i].to_value() as &gtk::Value);
			tree_store.set_value(&iter, 3, &parsed[3][i].to_value() as &gtk::Value);
		}
		// ここでtree_storeが破棄されるので、以後tree_storeはいじれない
		// cloneしてない場合はchooserも
	});

	window.show_all();
}

fn append_column(tree: &gtk::TreeView, id: i32) {
	let title = ["Title", "Start time", "End time", "Time gap"];
	let column = gtk::TreeViewColumn::new();
	let cell = gtk::CellRendererText::new();
	column.pack_start(&cell, true);
	column.add_attribute(&cell, "text", id);
	column.set_title(title[id as usize]);
	tree.append_column(&column);
}

fn parse(d: &str) -> DateTime<Local> {
	let regex = Regex::new(r"(\d{4}).[ \t\n\r\f\v]+(\d+)+.[ \t\n\r\f\v]+(\d+)+.[ \t\n\r\f\v]+(\S+)[ \t\n\r\f\v]+(\d{2}:\d{2}:\d{2})[ \t\n\r\f\v]+(\S[^,]+)").unwrap();
	let cap = regex.captures(d).unwrap();

	let year: i32 = cap[1].parse().unwrap();
	let month: u32 = cap[2].parse().unwrap();
	let day: u32 = cap[3].parse().unwrap();
	let date = &cap[5];
	let dc: Vec<&str> = date.split(':').collect();
	let h: u32 = dc[0].parse().unwrap();
	let m: u32 = dc[1].parse().unwrap();
	let s: u32 = dc[2].parse().unwrap();

	Local.ymd(year, month, day).and_hms(h, m, s)
}

fn read_log(name: std::path::PathBuf) -> Vec<String> {
	let f = File::open(&name).unwrap();
	let reader = BufReader::new(f);
	let reg_title = Regex::new(REG_TITLE).unwrap();
	let reg_time = Regex::new(REG_TIME).unwrap();
	let mut log: Vec<String> = Vec::new();
	for line in reader.lines() {
		let l = line.unwrap();
		if reg_title.is_match(&l) {
			let t_reg = Regex::new(r"\./").unwrap();
			let title = t_reg.replace_all(&l, "");
			log.push(title.to_string());
		}
		if reg_time.is_match(&l) {
			for n in reg_time.find_iter(&l) {
				log.push(n.as_str().to_string());
			}
		}
	}
	log
}

fn parse_log(log: Vec<String>) -> Vec<Vec<String>> {
	let reg_title = Regex::new(REG_TITLE).unwrap();
	let reg_time = Regex::new(REG_TIME).unwrap();
	let mut ret: Vec<Vec<String>> = Vec::new();
	let mut title: Vec<String> = Vec::new();
	let mut start_t: Vec<String> = Vec::new();
	let mut end_t: Vec<String> = Vec::new();
	let mut diff_t: Vec<String> = Vec::new();
	let mut times = false;
	for i in 0..log.len() {
		if reg_title.is_match(&log[i]) {
			if DEBUG {
				println!("{}", log[i]);
			}
			title.push(log[i].to_string());
			if times {
				end_t.push("None".to_string());
				diff_t.push("None".to_string());
				times = false;
			}
		}
		if reg_time.is_match(&log[i]) {
			if !times {
				if DEBUG {
					println!("{}", log[i]);
				}
				start_t.push(log[i].to_string());
				times = true;
			} else {
				let start = parse(&log[i - 1]);
				let end = parse(&log[i]);
				let diff = end.timestamp() - start.timestamp();
				let du = Duration::seconds(diff as i64);
				if DEBUG {
					println!("{}", log[i]);
				}
				end_t.push(log[i].to_string());
				diff_t.push(format!("{}days {:02}h {:02}m {:02}s",
					du.num_days(),
					du.num_hours() % 24,
					du.num_minutes() % 60,
					du.num_seconds() % 60
				));
				if DEBUG {
					println!("{}", format!("{}days {:02}h {:02}m {:02}s",
						du.num_days(),
						du.num_hours() % 24,
						du.num_minutes() % 60,
						du.num_seconds() % 60
					));
				}
				times = false;
			}
		}
	}
	if times {
		end_t.push("None".to_string());
		diff_t.push("None".to_string());
	}
	ret.push(title);
	ret.push(start_t);
	ret.push(end_t);
	ret.push(diff_t);
	ret
}

