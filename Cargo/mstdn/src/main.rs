#[allow(dead_code)]
extern crate mammut;
extern crate toml;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate reqwest;

use std::fs::File;
use std::io::prelude::*;
use std::thread;
use std::io::{BufReader, stdout, Write, StdoutLock};
use std::error::Error;
use mammut::{Data, Mastodon, Registration};
use mammut::apps::{AppBuilder, Scopes};
use mammut::entities::status::Status;
use mammut::entities::notification::Notification;

fn main() {
	let mastodon = match File::open("mastodon-data.toml") {
		Ok(mut file) => {
			let mut config = String::new();
			match file.read_to_string(&mut config) {
				Ok(f) => f,
				Err(e) => panic!("{}", e.description()),
			};
			let data: Data = match toml::from_str(&config) {
				Ok(o) => o,
				Err(e) => panic!("{}", e.description()),
			};
			Mastodon::from_data(data)
		},
		Err(_) => register(),
	};
	/*
	let mut fav = mastodon.notifications().unwrap();
	fav.next_page();
	fav.prev_page();
//	fav.next_page();

	for f in fav.next_page().unwrap().unwrap().iter().rev() {
		println!("{:?}", f);
//		print_toot(&f, false);
	}
	*/
	/*
	for f in fav.unwrap().iter().rev() {
		print_toot(f, false);
	}
	*/
//	println!("{:?}")
//k	println!("{:?}", fav.next_page());
	streaming(mastodon.data);
}

fn streaming(d: mammut::Data) {
	let client = reqwest::Client::new();
	let auth = reqwest::header::HeaderValue::from_str(&format!("Bearer {}", d.token));
	let res = match client
		.get(&format!("{}/api/v1/streaming/public", d.base))
		.header(reqwest::header::AUTHORIZATION, auth.unwrap())
		.send() {
		Ok(o) => o,
		Err(e) => panic!("{}", e.description()),
	};
	if res.status().is_success() {
		let r_loop = thread::spawn(move || {
			let w = stdout();
			let mut w = w.lock();

			let mut event_type: Option<String> = None;
			let mut data: Option<String> = None;
			let res_buf = BufReader::new(res);
			for line in res_buf.lines() {
				let l = line.unwrap();
				if l.is_empty() {
					//
					match (event_type.clone(), data.clone()) {
						(Some(e), Some(d)) => {
							if e == "update" {
								let s: mammut::entities::status::Status =
								match serde_json::from_str(&d) {
									Ok(o) => o,
									Err(e) => panic!("{}", e.description()),
								};
								print_toot(&mut w, &s, false);
								writeln!(w, "").unwrap();
							} else if e == "notification" {
								let n: mammut::entities::notification::Notification =
								match serde_json::from_str(&d) {
									Ok(o) => o,
									Err(e) => panic!("{}", e.description()),
								};
								print_notification(&mut w, n);
								writeln!(w, "").unwrap();
							} else if e == "delete" {
								writeln!(w, "delete: {}", d).unwrap();
							} else {
								writeln!(w, "EventType: {}\nData: {}", e, d).unwrap();
							}
						},
						_ => {
							writeln!(w, "Data broken!").unwrap();
						},
					}
				} else if l.starts_with(":") {
				} else if l.starts_with("event:") {
					// 順番的には最初に来るはず
					// 順番に来てないデータは壊れてる
					event_type = Some(l.trim_left_matches("event: ").to_string());
				} else if l.starts_with("data:") {
					// イベントの種類がきてからデータがくる
					data = Some(l.trim_left_matches("data: ").to_string());
				}
			}
		});
		let _ = r_loop.join();
	}
}

fn print_notification(w: &mut StdoutLock, n: mammut::entities::notification::Notification) {
	writeln!(w, "{} {}", n.account.acct, n.account.display_name).unwrap();
	writeln!(w, "{:?}", n.notification_type).unwrap();
}

/*
fn print_timeline(tl: std::vec::Vec<mammut::entities::status::Status>) {
	for post in tl {
		print_toot(&mut w, &post, false);
		post.application.map(|app| {
			println!("via {} ({})", app.name, match app.website {
				Some(s) => s,
				None => "None".to_string(),
			});
		});
		println!("");
	}
}
*/

fn print_toot(w: &mut StdoutLock, toot: &mammut::entities::status::Status, boost: bool) {
	writeln!(w, "{}{} ({})", boosted_toot(boost), toot.account.display_name, toot.account.acct).unwrap();
	writeln!(w, "{}id: {}", boosted_toot(boost), toot.id).unwrap();
	writeln!(w, "{}Date {:?}", boosted_toot(boost), toot.created_at).unwrap();
	match toot.reblog {
		Some(ref r) => {
			print_toot(w, &r, true);
		},
		None => {
			writeln!(w, "{}{}", boosted_toot(boost), toot.content);
		},
	}
	/*
	for media in toot.media_attachments {
		println!("{}media no.{}", boosted_toot(boost), media.id);
		println!("{}{:?}", boosted_toot(boost), media.media_type);
		println!("{}{}", boosted_toot(boost), media.url);
	}
	*/
}

fn boosted_toot(b: bool) -> &'static str{
	if b {
		"\t"
	} else {
		""
	}
}

fn register() -> Mastodon {
	let app = AppBuilder {
		client_name: "mammut-example",
		redirect_uris: "urn:ietf:wg:oauth:2.0:oob",
		scopes: Scopes::All,
		website: Some("https://github.com/inux39/-"),
	};

	let mut registration = Registration::new("https://mstdn.jp");
	match registration.register(app) {
		Ok(_) => {},
		Err(e) => panic!("{}", e.description()),
	};

	let url = match registration.authorise() {
		Ok(o) => o,
		Err(e) => panic!("{}", e.description()),
	};

	println!("Click this link to authorize on Mastodon: {}", url);
	println!("Paste the returned authorization code: ");

	let mut input = String::new();
	std::io::stdin().read_line(&mut input).unwrap();

	let code = input.trim();
	let mastodon = registration.create_access_token(code.to_string()).unwrap();


	// save app data
	let toml = toml::to_string(&*mastodon).unwrap();
	let mut file = File::create("mastodon-data.toml").unwrap();
	file.write_all(toml.as_bytes()).unwrap();
	mastodon
}

