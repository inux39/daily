extern crate gif;
extern crate clap;

use std::io::{Read, Write};
use std::fs::File;
use std::io::BufWriter;
use clap::{Arg, App};
use gif::{Decoder, Encoder};
use gif::SetParameter;

fn main() {
	/*
	let cmd = App::new(env!("CARGO_PKG_NAME"))
		.version(env!("CARGO_PKG_VERSION"))
		.author(env!("CARGO_PKG_AUTHORS"))
		.arg(Arg::with_name("input")
			.takes_value(true))
		.arg(Arg::with_name("output")
			.takes_value(true))
		.arg(Arg::with_name("encode")
			.short("e"))
		.arg(Arg::with_name("decode")
			.short("d"))
		.get_matches();
	*/
//	encode("input".to_string(), "output.gif".to_string());
	decode("input.gif".to_string(), "output".to_string());
}

fn encode(i: String, o: String) {
	let mut file = File::open(i).unwrap();
	let mut data = String::new();
	file.read_to_string(&mut data).unwrap();
	let mut data = data.into_bytes();
	let fsize = 100;
	while data.len() < (fsize * fsize * 4) {
		data.push(0);
	}

	let frame = gif::Frame::from_rgba(fsize as u16, fsize as u16, &mut *data);
	let mut image = File::create(o).unwrap();
	let mut enc = Encoder::new(&mut image, frame.width, frame.height, &[]).unwrap();
	enc.write_frame(&frame).unwrap();
}

fn decode(i: String, o: String) {
	let file = File::open(i).unwrap();
	let mut dec = Decoder::new(file);
	let mut dec = dec.read_info().unwrap();
	while let Some(frame) = dec.read_next_frame().unwrap() {
		let f = frame.clone();
		println!("{:02x?}", f.buffer);
	}
}

