package main
import (
	"fmt"
	"testing"
)

func main() {
	fmt.Println(is_uru(1600), "== true");
	fmt.Println(is_uru(1700), "== false");
	fmt.Println(is_uru(2000), "== true");
	fmt.Println(is_uru(2500), "== false");
}

func is_uru(y int) bool {
	if y % 4 == 0 {
		if y % 100 == 0 && y % 400 != 0 {
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

func uru_string(b bool) string {
	if b {
		return "うるう年"
	} else {
		return "うるう年ではない"
	}
}

func TestUru(t *testing.T) {
	y1600 := is_uru(1600);
	e1600 := true;
/*
	fmt.Println(is_uru(1600), "== true");
	fmt.Println(is_uru(1700), "== false");
	fmt.Println(is_uru(2000), "== true");
	fmt.Println(is_uru(2500), "== false");
*/
	if y1600 != e1600 {
		t.Errorf("%v, %v", y1600, e1600);
	}
}

