package main
import ("fmt"; "strconv")

func main() {
	var x = 100;
	for i := 1; i < x; i++ {
		fmt.Println(fizzbuzz(i));
	}
}

func fizzbuzz(i int) string {
	if i % 3 == 0 && i % 5 == 0{
		return "FizzBuzz";
	} else if i % 3 == 0 {
		return "Fizz";
	} else if i % 5 == 0 {
		return "Buzz";
	} else {
		return strconv.Itoa(i);
	}
}

