[Go](https://golang.org)
===
## Documents
- [Effective Go - The Go Programming Language](https://golang.org/doc/effective_go.html)

## Run
`go run ${FILE}`

