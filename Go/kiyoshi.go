package main;
import (
	"fmt"
	"math/rand"
)

func main() {
	var c int = 0;
	for {
		var r int = rand.New(rand.NewSource(50)).Int();
		var rnd bool = r % 2 == 0;
		if(rnd) { fmt.Print("DOKO "); } else { fmt.Print("ZUN "); }
		if(c == 4 && rnd) {
			fmt.Println("KI YO SHI!");
			break;
		}

		if(!rnd) { c += 1; } else { c = 0; }
	}
}

